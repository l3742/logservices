package ar.com.redlink.logservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.redlink.logservices.model.LogIVRDetalle;

@Repository
public interface LogIVRDetalleRepository extends JpaRepository<LogIVRDetalle, Long>{

}