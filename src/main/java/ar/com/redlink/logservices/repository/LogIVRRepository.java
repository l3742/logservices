package ar.com.redlink.logservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ar.com.redlink.logservices.model.LogIVR;

@Repository
public interface LogIVRRepository extends JpaRepository<LogIVR, Long>{

}