package ar.com.redlink.logservices.controller;

import javax.ws.rs.BadRequestException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.com.redlink.logservices.model.dto.request.LogIVRDetalleDto;
import ar.com.redlink.logservices.model.dto.request.LogIVRDto;
import ar.com.redlink.logservices.model.dto.request.LogResumenDto;
import ar.com.redlink.logservices.service.LogIVRDetalleService;
import ar.com.redlink.logservices.service.LogIVRService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/logs")
@Validated
public class LogIVRController {
	
	@Autowired
	private LogIVRService logIVRService;
	
	@Autowired
	private LogIVRDetalleService logIVRDetalleService;
	
	
	@ApiOperation(value = "Guarda informacion inicial de una llamada telefonica")
	@PostMapping("/llamadas")
	public void registrarLogLlamada(@RequestHeader("X-IdRequerimiento") String xIdRequerimiento, 
									@RequestHeader("Authorization") String authorization,
									@RequestHeader("ipCliente") String ipCliente,
									@RequestHeader("timestamp") String timeStamp,
									@RequestBody LogIVRDto logIVRDto) throws BadRequestException{
		
		this.logIVRService.guardarRegistro(logIVRDto);

	}
	
	@ApiOperation(value = "Guarda informacion de detalles de llamadas telefonicas")
	@PostMapping("/detalles")
	public void registrarLogLlamadaDetalle(@RequestHeader("X-IdRequerimiento") String xIdRequerimiento, 
									       @RequestHeader("Authorization") String authorization,
									       @RequestHeader("ipCliente") String ipCliente,
									       @RequestHeader("timestamp") String timeStamp,
									       @RequestBody LogIVRDetalleDto logIVRDetalleDto) throws Exception{
		this.logIVRDetalleService.guardarRegistro(logIVRDetalleDto);
	}
	
	@ApiOperation(value = "Guarda logs de resumen de llamadas telefonicas del sistema Genesys")
	@PostMapping("/resumen")
	public void registrarLogResumen(@RequestHeader("X-IdRequerimiento") String xIdRequerimiento, 
									@RequestHeader("Authorization") String authorization,
									@RequestHeader("ipCliente") String ipCliente,
									@RequestHeader("timestamp") String timeStamp,
									@RequestBody LogResumenDto logResumenDto) throws Exception{
		this.logIVRService.guardarResumen(logResumenDto);
	
	}
}
