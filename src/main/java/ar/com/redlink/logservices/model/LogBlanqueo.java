package ar.com.redlink.logservices.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "LOG_BLANQUEO")
public class LogBlanqueo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G_LogBlanqueo")
	@SequenceGenerator(name = "G_LogBlanqueo", allocationSize = 1, sequenceName = "SEQ_LOG_BLANQUEO")
	@Column(name = "LOG_BLANQUEO_ID")
	private Long idLogBlanqueo;
	
	@Column(name = "LOG_IVR_ID")
	private Long idLogIvr;

	@Column(name = "TIPO_TERMINAL")
	private String tipoTerminal;

	@Column(name = "NRO_TERMINAL")
	private String numeroTerminal;
	
	@Column(name = "RESULTADO")
	private String resultado;
	
	@Column(name = "ID_VAL_POSITIVA")
	private Long idValidacionPositiva;
	
	@Column(name = "ESTADO_VAL_POSITIVA")
	private String estadoValidacion;
	
	@Column(name = "PAN")
	private String pan;
	
	@Column(name = "FIID")
	private String fiid;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
}