package ar.com.redlink.logservices.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogResumenDto {

	private Long logServiceId;
	private String resumenLlamada;

}
