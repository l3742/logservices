package ar.com.redlink.logservices.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "LOG_IVR")
public class LogIVR {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G_LogIvr")
	@SequenceGenerator(name = "G_LogIvr", allocationSize = 1, sequenceName = "SEQ_LOG_IVR")
	@Column(name = "LOG_IVR_ID")
	private Long idLogIvr;

	@Column(name = "IDENTIFICADOR_GENESYS")
	private String identificadorGenesys;

	@Column(name = "ANI")
	private String ani;

	@Column(name = "DNIS")
	private String dnis;
	
	@Column(name = "TIPO_OPERACION_ID")
	private Long tipoOperacion;
	
	@Column(name = "RESUMEN_LOG")
	private String resumenLog;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@OneToMany(mappedBy = "logIVR", cascade = CascadeType.ALL)
	private Set<LogIVRDetalle> logIVRDetalle = new HashSet<LogIVRDetalle>();
	
}
