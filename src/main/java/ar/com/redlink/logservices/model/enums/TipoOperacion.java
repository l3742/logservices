package ar.com.redlink.logservices.model.enums;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TIPO_OPERACION")
public class TipoOperacion {
	
	@Id
	@Column(name = "TIPO_OPERACION_ID")
	private Long idTipoOperacion;

	@Column(name = "OPERACION")
	private String operacion;

	@Column(name = "DESCRIPCION")
	private String descripcion;

}
