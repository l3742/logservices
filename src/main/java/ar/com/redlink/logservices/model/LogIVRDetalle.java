package ar.com.redlink.logservices.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "LOG_IVR_DETALLE")
public class LogIVRDetalle {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G_LogIvrDetalle")
	@SequenceGenerator(name = "G_LogIvrDetalle", allocationSize = 1, sequenceName = "SEQ_LOG_IVR_DETALLE")
	@Column(name = "LOG_IVR_DETALLE_ID")
	private Long idLogIVRDetalle;

	@Column(name = "TIPO_SISTEMA_ID")
	private Long idTipoSistema;
	
	@Column(name = "ACCION_REALIZADA")
	private String accionRealizada;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "LOG_IVR_ID")
	private LogIVR logIVR;
	
}
