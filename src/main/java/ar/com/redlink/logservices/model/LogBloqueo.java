package ar.com.redlink.logservices.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "LOG_BLOQUEO")
public class LogBloqueo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "G_LogBloqueo")
	@SequenceGenerator(name = "G_LogBloqueo", allocationSize = 1, sequenceName = "SEQ_LOG_BLOQUEO")
	@Column(name = "LOG_BLOQUEO_ID")
	private Long idLogBloqueo;
	
	@Column(name = "LOG_IVR_ID")
	private Long idLogIvr;
	
	@Column(name = "NRO_DOCUMENTO")
	private String nroDocumento;
	
	@Column(name = "TIPO_DOCUMENTO")
	private String tipoDocumento;
	
	@Column(name = "PAN")
	private String pan;

	@Column(name = "FIID")
	private String fiid;
	
	@Column(name = "NRO_DENUNCIA")
	private String numeroDenuncia;

	@Column(name = "FECHA_CREACION")
	private Date fechaCreacion;

}
