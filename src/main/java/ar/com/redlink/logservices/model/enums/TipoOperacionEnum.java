package ar.com.redlink.logservices.model.enums;

import java.io.Serializable;

public enum TipoOperacionEnum implements Serializable {
	
	BP(1l, "Blanqueo de PIL"),
	BT(2l, "Bloqueo de tarjeta"),
	CD(3l, "Consulta de denuncia");
	
	private long id;
	private String descripcion;
	
	private TipoOperacionEnum(long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public long getId() {
		return id;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public static String getEstado(long id){
		for(TipoOperacionEnum estado : TipoOperacionEnum.values()){
			if(estado.getId() == id){
				return estado.getDescripcion();
			}
		}
		return null;
	}

}
