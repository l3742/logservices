package ar.com.redlink.logservices.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorMsgDto {
	public ErrorMsgDto(String codigo,String descripcion){
        this.codigo = codigo;
        this.descripcion = descripcion;
    }
    public ErrorMsgDto(){

    }
    private String codigo;
    private String descripcion;
}
