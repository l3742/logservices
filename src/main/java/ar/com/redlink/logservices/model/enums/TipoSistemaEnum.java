package ar.com.redlink.logservices.model.enums;

import java.io.Serializable;

public enum TipoSistemaEnum implements Serializable {

	IVR(1l, "Servicio que realiza las acciones invocadas por Genesys"),
	GENESYS(2l, "Servicio externo para IVR");
	
	private long id;
	private String descripcion;
	
	private TipoSistemaEnum(long id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public long getId() {
		return id;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	public static String getEstado(long id){
		for(TipoSistemaEnum estado : TipoSistemaEnum.values()){
			if(estado.getId() == id){
				return estado.getDescripcion();
			}
		}
		return null;
	}
}
