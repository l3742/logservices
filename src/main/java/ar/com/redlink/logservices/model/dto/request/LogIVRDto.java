package ar.com.redlink.logservices.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogIVRDto {

	private String idLlamada;
	private String ani;
	private String dnis;
	private String tipoOperacion;

}
