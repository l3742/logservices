package ar.com.redlink.logservices.model.enums;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TIPO_SISTEMA")
public class TipoSistema {

	@Id
	@Column(name = "TIPO_SISTEMA_ID")
	private Long idTipoSistema;

	@Column(name = "SISTEMA")
	private String sistema;

	@Column(name = "DESCRIPCION")
	private String descripcion;
	
}
