package ar.com.redlink.logservices.model.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LogIVRDetalleDto {

	private Long logServiceId;
	private String tipoSistema;
	private String accion;

}
