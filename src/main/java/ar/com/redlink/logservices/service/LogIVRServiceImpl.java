package ar.com.redlink.logservices.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.redlink.logservices.model.LogIVR;
import ar.com.redlink.logservices.model.dto.request.LogIVRDto;
import ar.com.redlink.logservices.model.dto.request.LogResumenDto;
import ar.com.redlink.logservices.repository.LogIVRRepository;

@Service
public class LogIVRServiceImpl implements LogIVRService{
	
	@Autowired
	private LogIVRRepository repoLogIVR;


	@Override
	public void guardarRegistro(LogIVRDto logIVRDto) {
		
		LogIVR logIVR = new LogIVR();
		
		logIVR.setIdentificadorGenesys(logIVRDto.getIdLlamada());
		logIVR.setAni(logIVRDto.getAni());
		logIVR.setDnis(logIVRDto.getDnis());
		logIVR.setTipoOperacion(Long.valueOf(logIVRDto.getTipoOperacion()));

		logIVR.setFechaCreacion(new Date());
		
		this.repoLogIVR.save(logIVR);
	}

	@Override
	public void guardarResumen(LogResumenDto logResumenDto) {
			
		LogIVR logIVR = this.repoLogIVR.getById(logResumenDto.getLogServiceId());
		logIVR.setResumenLog(logResumenDto.getResumenLlamada());
		
		this.repoLogIVR.save(logIVR);
		
	}

}
