package ar.com.redlink.logservices.service;

import ar.com.redlink.logservices.model.dto.request.LogIVRDto;
import ar.com.redlink.logservices.model.dto.request.LogResumenDto;


public interface LogIVRService {
	
	public void guardarRegistro(LogIVRDto logIVRDto);
	public void guardarResumen(LogResumenDto logResumenDto);
	

}
