package ar.com.redlink.logservices.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.com.redlink.logservices.model.LogIVR;
import ar.com.redlink.logservices.model.LogIVRDetalle;
import ar.com.redlink.logservices.model.dto.request.LogIVRDetalleDto;
import ar.com.redlink.logservices.repository.LogIVRDetalleRepository;
import ar.com.redlink.logservices.repository.LogIVRRepository;

@Service
public class LogIVRDetalleServiceImpl implements LogIVRDetalleService{
	
	@Autowired
	private LogIVRDetalleRepository repoLogIVRDetalle;
	
	@Autowired
	private LogIVRRepository repoLogIVR;
	
	@Override
	public void guardarRegistro(LogIVRDetalleDto logIVRDetalleDto) {
		
		LogIVRDetalle logIVRDetalle = new LogIVRDetalle();
		//LogIVR logIVR = this.repoLogIVR.getById(logIVRDetalleDto.getLogServiceId());
		LogIVR logIVR = this.repoLogIVR.getById(logIVRDetalleDto.getLogServiceId());
		
		logIVRDetalle.setLogIVR(logIVR);
		logIVRDetalle.setIdTipoSistema(Long.valueOf(logIVRDetalleDto.getTipoSistema()));
		logIVRDetalle.setAccionRealizada(logIVRDetalleDto.getAccion());
		logIVRDetalle.setFechaCreacion(new Date());
		
		this.repoLogIVRDetalle.save(logIVRDetalle);
		
	}

}
