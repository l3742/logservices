package ar.com.redlink.logservices.service;

import ar.com.redlink.logservices.model.dto.request.LogIVRDetalleDto;

public interface LogIVRDetalleService {
	
	public void guardarRegistro(LogIVRDetalleDto logIVRDetalleDto);

}
