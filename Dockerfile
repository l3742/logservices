FROM image-registry.openshift-image-registry.svc:5000/link-app-ivr-desa/maven as build
WORKDIR /app
COPY . .
RUN mvn -s ci_settings.xml clean  install -X -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.test.skip=true

#
# Package stage
#

#FROM openjdk:11-jre-slim
FROM image-registry.openshift-image-registry.svc:5000/link-app-ivr-desa/openjdk:8-jre-slim
#FROM image-registry.openshift-image-registry.svc:5000/link-app-ivr-desa/openjdk-link:11-jre-slim
COPY --from=build /app/target/*.jar /usr/local/lib/app.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/usr/local/lib/app.jar"]
